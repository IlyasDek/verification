package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.model.dto.QuestionCheckRequest;
import com.m46.codechecks.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/home")
public class QuestionsCheckController {
    private final QuestionService questionService;

    @GetMapping(path = "/question")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<QuestionCheckRequest> get() {
        QuestionCheckRequest question = questionService.getAllQuestion();
        return ResponseEntity.ok(question);
    }

    @PostMapping(path = "/answer")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> getAnswer(@RequestBody Question question) {
        System.out.println("ID CONTROLLER " + question.toString());
        boolean bool = questionService.getQuestion(question);

        if (!bool) {
            RestTemplate restTemplate = new RestTemplate();

            String url = "http://161.35.206.90:8082/api/home/question";

            return ResponseEntity.ok(restTemplate.getForObject(url, String.class));
        }
        System.out.println("QUESTION: " + question.toString());
        return ResponseEntity.ok(bool);
    }
}
