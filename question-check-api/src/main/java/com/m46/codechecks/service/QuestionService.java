package com.m46.codechecks.service;

import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.model.dto.QuestionCheckRequest;
import com.m46.codechecks.model.entity.QuestionEntity;
import com.m46.codechecks.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QuestionService {

    private final QuestionRepository questionRepository;
    private final ModelMapper modelMapper;



    public QuestionCheckRequest getAllQuestion() {
        List<QuestionEntity> questionList = questionRepository.findAll();
        QuestionEntity questionEntity =  questionList.get((int)(1+Math.random() * questionList.size() - 1));
        Question question = modelMapper.map(questionEntity, Question.class);
        QuestionCheckRequest questionCheckRequest = new QuestionCheckRequest();
        questionCheckRequest.setQuestion(question.getQuestion());
        questionCheckRequest.setQuestionId(question.getQuestionId());
        return questionCheckRequest;
    }

    public boolean getQuestion(Question question) {
        Optional<QuestionEntity> questionEntity = questionRepository.findById(question.getQuestionId());

//        Question question1 = modelMapper.map(questionEntity,Question.class);

        if(!questionEntity.get().getAnswer().equals(question.getAnswer())) {
            return false;
        }
        return true;
    }
}
