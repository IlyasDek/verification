package com.m46.codechecks.controller;

import com.m46.codechecks.model.EmailVerification;
import com.m46.codechecks.model.EmailVerificationState;
import com.m46.codechecks.model.VerificationRequest;
import com.m46.codechecks.service.EmailVerificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/email")
public class EmailVerificationController {
    private final EmailVerificationService verificationService;

    @GetMapping(path = "/{email}")
    @ResponseStatus(HttpStatus.OK)
    public EmailVerificationState getVerificationState(
            @PathVariable String email
    ){

        return verificationService.getVerificationState(email);
    }

    @PostMapping(path = "/send")
    @ResponseStatus(HttpStatus.CREATED)
    public void requestEmailVerification(
            @Validated
            @RequestBody VerificationRequest verificationRequest
    ) {
        log.info("New verificatio for {} {}", verificationRequest.getFirstName(),
                verificationRequest.getEmail());
        verificationService.requestEmailVerification(
                verificationRequest.getEmail(),
                verificationRequest.getFirstName());
        System.out.println("VERIFICATION " + verificationRequest.toString());
    }

    @PostMapping(path = "/check")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity verify(
            @Validated
            @RequestBody EmailVerification verification
    ) {
        log.info("Verify {} with code '{}'", verification.getEmail(), verification.getVerificationCode());
        verificationService.verify(verification.getEmail(), verification.getVerificationCode());
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://161.35.206.90:8082/api/home/question";
        return ResponseEntity.ok(restTemplate.getForObject(url,String.class));
    }
}
