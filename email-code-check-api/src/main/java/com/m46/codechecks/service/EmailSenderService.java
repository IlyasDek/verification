package com.m46.codechecks.service;

import com.m46.codechecks.config.EmailCodeCheckProps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final EmailCodeCheckProps verificationProps;
    private final JavaMailSender javaMailSender;

    public void sendVerificationEmail(String email, String customerName, String code) {
        sendEmail(email, customerName, code, verificationProps.getEmailTemplateId());
    }

    public void sendEmail(String email, String customerName, String code, Long templateId) {
        //TODO
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("ilyas.dekiyev@gmail.com");
        message.setTo(email);
        message.setSubject("Hi, " + customerName);
        message.setText("Your verification code is: " + code);
        javaMailSender.send(message);
    }
}
