package com.m46.codechecks.service;

import com.m46.codechecks.model.dto.PersonDto;
import com.m46.codechecks.model.entity.PersonEntity;
import com.m46.codechecks.model.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonService {
    private final ModelMapper modelMapper;
    private final PersonRepository personRepository;

    public PersonDto addPerson(PersonDto personDto) {
        System.out.println("ServiceDto " + personDto.toString());
        PersonEntity personEntity = personRepository.save(modelMapper.map(personDto,
                PersonEntity.class));
        personDto = modelMapper.map(personEntity, PersonDto.class);
        System.out.println("ServiceDto1 " + personDto.toString());
        return personDto;
    }
}
