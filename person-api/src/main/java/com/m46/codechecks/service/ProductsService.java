package com.m46.codechecks.service;

import com.m46.codechecks.model.dto.ProductsDto;
import com.m46.codechecks.model.entity.ProductsEntity;
import com.m46.codechecks.model.repository.ProductsRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductsService {

    private final ModelMapper modelMapper;
    private final ProductsRepository productsRepository;
    public void addProducts(ProductsDto productsDto) {
       productsRepository.save(modelMapper.map(productsDto, ProductsEntity.class));
    }
}
