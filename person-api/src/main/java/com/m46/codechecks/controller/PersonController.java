package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.EmailVerifiCodeDto;
import com.m46.codechecks.model.dto.PersonDto;
import com.m46.codechecks.service.PersonService;
import com.m46.codechecks.service.ProductsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/add")
public class PersonController {
    private final PersonService personService;
    private final ProductsService productService;


    @PostMapping(path = "/send")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> personController(@RequestBody PersonDto personDto) {
        System.out.println("person sending " + personDto.toString());
        EmailVerifiCodeDto verifiCodeDto = new EmailVerifiCodeDto();
        verifiCodeDto.setEmail(personDto.getEmail());
        verifiCodeDto.setFirstName(personDto.getName());

        RestTemplate restTemplate = new RestTemplate();

        try {

            String url = "http://161.35.206.90:8081/api/email/send";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<?> entity = new HttpEntity<>(verifiCodeDto, headers);
            System.out.println("QUESTION: "+verifiCodeDto.getEmail()+","+verifiCodeDto.getFirstName()+","+personDto.getProductsDto().getName()+","+personDto.getProductsDto().getStatus());
            restTemplate.postForObject(url, entity, String.class);

        }catch (RuntimeException e){
            throw new RuntimeException("ERROR EMAIL");
        }

        PersonDto personDto1 = personService.addPerson(personDto);

        if (Objects.nonNull(personDto1)) {
            System.out.println("PRODUCTS " + personDto.getProductsDto().toString());
            int ID = personDto1.getId();
            System.out.println("ID");
            personDto.getProductsDto().setPersonId(ID);
            System.out.println("PRODUCTS " + personDto.getProductsDto().toString());
            productService.addProducts(personDto.getProductsDto());
            return ResponseEntity.ok(personDto1);
        }else {
            throw new RuntimeException("Error add person");
        }

    }
}
